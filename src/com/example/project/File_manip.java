package com.example.project;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;

public class File_manip {
	
	static String sec_name = null;
	static String url = null;
	static String name = null;
	static int h=0;
	static Context c;
	static List<String> Categories;
	static List<String> Thumbs_links;
	public static void ManageSection(Context context,List<String> ids,List<String> categories,List<String> thumbs_links){
		Categories=categories;
		Thumbs_links=thumbs_links;
		String dir = Environment.getExternalStorageDirectory()+"/"+"State";
		File file = new File(dir, "Sectionsids.txt");
		
		FileOutputStream fos=null;
		c=context;
		try {
			fos = new FileOutputStream(file);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} 
		
		for (String s : ids) {  
			 s=s+"\n";
			try {
				
				
				fos.write(s.getBytes());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}  
		} 
		try {
			fos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		file = new File(dir,"SectionsNames.txt");
		try {
			
			fos = new FileOutputStream(file);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	
		for (String s : categories) {  
			 s=s+"\n";
			try {
				fos.write(s.getBytes());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}  
		} 
		
		try {
			fos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		thread_imgs.start();
		
		
		
		
		
	}
	static Thread thread_imgs = new Thread(new Runnable(){
	    @Override
	    public void run() {
	        try {
	        	for(int i=0;i<Thumbs_links.size();i++)
	    		{
	    			url=Thumbs_links.get(i);
	        		name = Categories.get(i);
	    			downloadBitmap(url,name);
	    		}
	        	
	        	
	        }
	        catch (Exception e) {
	           
	        	e.printStackTrace();
	        }
	    }
	});
	

    /**
     * This method downloads bitmap image
     * @param url address of image
     * @throws IOException
     */
    public static void downloadBitmap(String url,String name) throws IOException {

        
        URL _url = new URL(url);
        
        HttpURLConnection connection = (HttpURLConnection) _url.openConnection();
        connection.setDoInput(true);
        connection.connect();
        InputStream input = connection.getInputStream();
        Bitmap myBitmap = BitmapFactory.decodeStream(input);
        String dir = Environment.getExternalStorageDirectory()+"/"+"State";
        String data1 = String.valueOf(String.format("%s.png",name));
        File mypath=new File(dir,data1);
        FileOutputStream stream = new FileOutputStream(mypath);
        ByteArrayOutputStream outstream = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.PNG, 85, outstream);
        byte[] byteArray = outstream.toByteArray();
        stream.write(byteArray);
        stream.close();
        
    }
    
    
    
    ///handling section books///
    public static void ManageSectionBooks(Context context,String sec,List<String> ids,List<String> titles,List<String> thumbs_links,List<String> authors,List<String> languages){
		Categories=titles;
		sec_name=sec;
		Thumbs_links=thumbs_links;
		String dir = Environment.getExternalStorageDirectory()+"/VillageApp"+"/"+sec;
		File file = new File(dir, "ids.txt");
		
		FileOutputStream fos=null;
		c=context;
		try {
			fos = new FileOutputStream(file);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} 
		
		for (String s : ids) {  
			 s=s+"\n";
			try {
				
				
				fos.write(s.getBytes());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}  
		} 
		try {
			fos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		file = new File(dir,"titles.txt");
		try {
			
			fos = new FileOutputStream(file);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	
		for (String s : titles) {  
			 s=s+"\n";
			try {
				fos.write(s.getBytes());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}  
		} 
		
		try {
			fos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		file = new File(dir,"authors.txt");
		try {
			
			fos = new FileOutputStream(file);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	
		for (String s : authors) {  
			 s=s+"\n";
			try {
				fos.write(s.getBytes());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}  
		} 
		
		try {
			fos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		file = new File(dir,"languages.txt");
		try {
			
			fos = new FileOutputStream(file);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	
		for (String s : languages) {  
			 s=s+"\n";
			try {
				fos.write(s.getBytes());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}  
		} 
		
		try {
			fos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		thread_imgs_book_cover.start();
		
		
		
		
		
	}
	static Thread thread_imgs_book_cover = new Thread(new Runnable(){
	    @Override
	    public void run() {
	        try {
	        	for(int i=0;i<Thumbs_links.size();i++)
	    		{
	        		url="https://www.iconfinder.com/icons/304419/download/png/256";
	    			name = Categories.get(i);
	    			downloadBitmapBookCover(url,name);
	    		}
	        	
	        	
	        }
	        catch (Exception e) {
	           
	        	e.printStackTrace();
	        }
	    }
	});
	

    /**
     * This method downloads bitmap image
     * @param url address of image
     * @throws IOException
     */
    public static void downloadBitmapBookCover(String url,String name) throws IOException {

        
        URL _url = new URL(url);
        
        HttpURLConnection connection = (HttpURLConnection) _url.openConnection();
        connection.setDoInput(true);
        connection.connect();
        InputStream input = connection.getInputStream();
        Bitmap myBitmap = BitmapFactory.decodeStream(input);
        String dir = Environment.getExternalStorageDirectory()+"/VillageApp"+"/"+sec_name;
        String data1 = String.valueOf(String.format("%s.png",name));
        File mypath=new File(dir,data1);
        FileOutputStream stream = new FileOutputStream(mypath);
        ByteArrayOutputStream outstream = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.PNG, 85, outstream);
        byte[] byteArray = outstream.toByteArray();
        stream.write(byteArray);
        stream.close();
        
    }
    
    
    
	public static List<String> splitthestrings(List<String> names){
		 
    	List<String> splitted = new ArrayList<String>();;
    	for(int i=0;i<names.size();i++){
    		
    		String temp = names.get(i);
    		String temp2="";
    		for(int j=0;j<temp.length();j++){
    			if(temp.charAt(j)!='.'){
    				temp2=temp2+temp.charAt(j);
    			}
    			else
    			break;
    			
    			
    		}
    		if(temp2.length()!=0)
    		{
    			
    			splitted.add(temp2);
    		}
    	}
    	
    	
    	return splitted; 	
    }
    
	public static List<String> getListOfFiles(String path) {

        File files = new File(path);

        FileFilter filter = new FileFilter() {

            private final List<String> exts = Arrays.asList("jpeg", "jpg",
                    "png", "bmp", "gif");

            @Override
            public boolean accept(File pathname) {
                String ext;
                String path = pathname.getPath();
                ext = path.substring(path.lastIndexOf(".") + 1);
                return exts.contains(ext);
            }
        };

        final File [] filesFound = files.listFiles(filter);
        List<String> list = new ArrayList<String>();
        if (filesFound != null && filesFound.length > 0) {
            for (File file : filesFound) {
               list.add(file.getName());
            }
        }
        Collections.sort(list, String.CASE_INSENSITIVE_ORDER);
        return list;
    }
	
	public static int getCountOfPages(String path,String page) {

        File files = new File(path);
        int counts=0;
        FileFilter filter = new FileFilter() {

            private final List<String> exts = Arrays.asList("jpeg", "jpg",
                    "png", "bmp", "gif");

            @Override
            public boolean accept(File pathname) {
                String ext;
                String path = pathname.getPath();
                ext = path.substring(path.lastIndexOf(".") + 1);
                return exts.contains(ext);
            }
        };

        final File [] filesFound = files.listFiles(filter);
       
        if (filesFound != null && filesFound.length > 0) {
            for (File file : filesFound) {
               
               if (file.getName().toLowerCase().contains(page.toLowerCase()))
               {
            	   counts++;
               }
            }
        }
        
        
        
        return counts;
    }
	
	
	public static  List<String> get_bok_id_name(Context context,String path) {
	
	List<String> names_ids=new ArrayList<String>();
	String dir = Environment.getExternalStorageDirectory()+"/"+"State";
	File file = new File(dir, path);
	
	
	 try {
		    
	
		        BufferedReader br = new BufferedReader(new FileReader(file));  
		        String line;   
		        while ((line = br.readLine()) != null) {
		        	       names_ids.add(line);
		                    
		                    } }
		    catch (IOException e) {
		        e.printStackTrace();
		        System.out.println("hello");           
	
		    }
	return names_ids;

		   
	}


}
