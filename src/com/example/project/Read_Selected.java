
	package com.example.project;

	import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;
import android.R.bool;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.ViewTreeObserver;
import android.webkit.WebSettings.ZoomDensity;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.view.MotionEvent;



	public class Read_Selected extends Activity
	{

	    //Globals:
	    private ImageView wv;
	    private int ViewSize = 0;
		String topic = "";
	    boolean switcch=false;
	    String page="";
	    String path="";
	    Bitmap bmp=null;
	    String recording=".mp3";
	    String rec="";
	    int page_no=0;
	    int total=0;
	    boolean music=false;
	    //player controls variable
	   
	    Button buttonPlayPause, buttonQuit;
	    TextView textState;
	    final MediaPlayer mediaPlayer = new MediaPlayer();
	    private int stateMediaPlayer;
	    private final int stateMP_Error = 0;
	    private final int stateMP_NotStarter = 1;
	    private final int stateMP_Playing = 2;
	    private final int stateMP_Pausing = 3;
	    //OnCreate Method:
	    @Override
	    protected void onCreate(Bundle savedInstanceState)
	    {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.activity_main_read);
	        
	        
	        Intent intent = getIntent();
	        
	        this.topic = intent.getStringExtra("Topic_name");
            total=File_manip.getCountOfPages(Environment.getExternalStorageDirectory() +"/"+"Download"+"/"+"Education"+"/",topic);
            
	        //Setup webview
	        wv = (ImageView)findViewById(R.id.webView1);
	       page=topic;
	       page_no=1;
	   	rec=topic;
		path=Environment.getExternalStorageDirectory() +"/"+"Download"+"/"+"Education"+"/"+page+page_no+".jpg";
	    recording=Environment.getExternalStorageDirectory() +"/"+"Download"+"/"+"Education"+"/"+rec+page_no+".mp3";


	        pdfLoadImages();//load images\
	        Button pro=(Button)findViewById(R.id.play);
	        Button pre=(Button)findViewById(R.id.b0);
	        buttonPlayPause = (Button)findViewById(R.id.b1);
	        Button in=(Button)findViewById(R.id.zoomin);
	        Button out=(Button)findViewById(R.id.zoomout);
	        pre.setOnClickListener(show_prev_page);
	        pre.setEnabled(false);
	        if(total==1){
	        pro.setEnabled(false);}
	        in.setOnClickListener(zoom_in);
	        out.setOnClickListener(zoom_out);
	        pro.setOnClickListener(show_page);
	        buttonPlayPause.setOnClickListener(buttonPlayPauseOnClickListener);
	        wv.setOnTouchListener(new View.OnTouchListener()
	        {

	            public final static int FINGER_RELEASED = 0;
	            public final static int FINGER_TOUCHED = 1;
	            public final static int FINGER_DRAGGING = 2;
	            public final static int FINGER_UNDEFINED = 3;

	            private int fingerState = FINGER_RELEASED;


	            @Override
	            public boolean onTouch(View view, MotionEvent motionEvent)
	            {

	                switch (motionEvent.getAction())
	                {

	                case MotionEvent.ACTION_DOWN:
	                    if (fingerState == FINGER_RELEASED)
	                    {

	                        fingerState = FINGER_TOUCHED;
	                        if(switcch==false)
	                        {
	                            RelativeLayout pr=(RelativeLayout)findViewById(R.id.r1);
	                            pr.setVisibility(RelativeLayout.VISIBLE);
	                            RelativeLayout pr2=(RelativeLayout)findViewById(R.id.r2);
	                            pr2.setVisibility(RelativeLayout.VISIBLE);
	                            switcch=true;
	                        }
	                        else
	                        {
	                            RelativeLayout pr=(RelativeLayout)findViewById(R.id.r1);
	                            pr.setVisibility(RelativeLayout.INVISIBLE);
	                            RelativeLayout pr2=(RelativeLayout)findViewById(R.id.r2);
	                            pr2.setVisibility(RelativeLayout.INVISIBLE);
	                            switcch=false;
	                        }
	                    }
	                    else
	                        fingerState = FINGER_UNDEFINED;

	                    break;

	                case MotionEvent.ACTION_UP:
	                    if(fingerState != FINGER_DRAGGING)
	                    {
	                        fingerState = FINGER_RELEASED;

	                        // Your onClick codes

	                    }
	                    else if (fingerState == FINGER_DRAGGING) fingerState = FINGER_RELEASED;
	                    else fingerState = FINGER_UNDEFINED;
	                    break;

	                case MotionEvent.ACTION_MOVE:
	                    if (fingerState == FINGER_DRAGGING)
	                    {
	                        fingerState = FINGER_DRAGGING;

	                      

	                    }

	                    else fingerState = FINGER_UNDEFINED;
	                    break;

	                default:
	                    fingerState = FINGER_UNDEFINED;

	                }

	                return false;
	            }
	        });



	    }

	    //Load Images:
	    private void pdfLoadImages()
	    {
	        try
	        {
	            // run async
	            new AsyncTask<Void, Void, Void>()
	            {
	                // create and show a progress dialog
	                ProgressDialog progressDialog = ProgressDialog.show(Read_Selected.this, "", "Opening...");

	                @Override
	                protected void onPostExecute(Void result)
	                {
	                    //after async close progress dialog
	                	wv.setImageBitmap(bmp);
	                    progressDialog.dismiss();
	                    initMediaPlayer();
	                }

	                @Override
	                protected Void doInBackground(Void... params)
	                {
	                    try
	                    {
	                    	
	                    	  bmp = BitmapFactory.decodeFile(path);
	               	       



	                    }
	                    catch (Exception e)
	                    {
	                        Log.d("error", e.toString());
	                    }
	                    return null;
	                }
	            } .execute();
	            System.gc();// run GC
	        }
	        catch (Exception e)
	        {
	            Log.d("error", e.toString());
	        }
	    }

	    private void initMediaPlayer()
	    {
	        String PATH_TO_FILE = recording;
	        

	        try
	        {
	            mediaPlayer.setDataSource(PATH_TO_FILE);
	            mediaPlayer.prepare();
	           
	            stateMediaPlayer = stateMP_NotStarter;
	        }
	        catch (IllegalArgumentException e)
	        {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	            
	            stateMediaPlayer = stateMP_Error;
	        }
	        catch (IllegalStateException e)
	        {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	            
	            stateMediaPlayer = stateMP_Error;
	        }
	        catch (IOException e)
	        {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	           
	            stateMediaPlayer = stateMP_Error;
	        }

	        mediaPlayer.start();
	        buttonPlayPause.setBackgroundResource(R.drawable.paause);
	        stateMediaPlayer = stateMP_Playing;
	        music=true;
	    }


	    //page button controls

	    Button.OnClickListener show_prev_page
	    = new Button.OnClickListener()
	    {

	        @Override
	        public void onClick(View v)
	        {
	            // TODO Auto-generated method stub
	            
	        	 if(page_no==2)
		            {

		                Button pro=(Button)findViewById(R.id.b0);
		                pro.setEnabled(false);
		            }
	            if(page_no==total)
	            {

	                Button pro=(Button)findViewById(R.id.play);
	                pro.setEnabled(true);
	            }
	            page_no--;
	            
	           
	           //load images\
	        	
	        	path=Environment.getExternalStorageDirectory() +"/"+"Download"+"/"+"Education"+"/"+page+page_no+".jpg";
	            recording=Environment.getExternalStorageDirectory() +"/"+"Download"+"/"+"Education"+"/"+rec+page_no+".mp3";
	        
	            pdfLoadImages();
	            mediaPlayer.reset();
	            initMediaPlayer();
	            
	        }

	    };

	    Button.OnClickListener show_page
	    = new Button.OnClickListener()
	    {

	        @Override
	        public void onClick(View v)
	        {
	            // TODO Auto-generated method stub
	           
	            if(page_no==1)
	            {

	                Button pro=(Button)findViewById(R.id.b0);
	                pro.setEnabled(true);
	            }
	            page_no++;
	            if(page_no==total)
	            {

	                Button pro=(Button)findViewById(R.id.play);
	                pro.setEnabled(false);

	            }
	            
	            path=Environment.getExternalStorageDirectory() +"/"+"Download"+"/"+"Education"+"/"+page+page_no+".jpg";
	            recording=Environment.getExternalStorageDirectory() +"/"+"Download"+"/"+"Education"+"/"+rec+page_no+".mp3";
	            pdfLoadImages();
	            
	            mediaPlayer.reset();
	            initMediaPlayer();
	            
	           

	        }
	    };

	    Button.OnClickListener zoom_out
	    = new Button.OnClickListener()
	    {

	        @Override
	        public void onClick(View v)
	        {
	            


	        }
	    };

	    Button.OnClickListener zoom_in
	    = new Button.OnClickListener()
	    {

	        @Override
	        public void onClick(View v)
	        {
	           


	        }
	    };


	    //player button controls

	    Button.OnClickListener buttonPlayPauseOnClickListener
	    = new Button.OnClickListener()
	    {

	        @Override
	        public void onClick(View v)
	        {
	        	if(mediaPlayer.isPlaying() ==true || mediaPlayer.isLooping() ==true )
	        	{
	        		onPause();
	                buttonPlayPause.setBackgroundResource(R.drawable.plaaay);
	        	}
	        	else
	        	{
	        		 mediaPlayer.start();
	                 buttonPlayPause.setBackgroundResource(R.drawable.paause);
	        	}
	            
	           
	        }
	    };

	    @Override
	    protected void onPause() {
	        // TODO Auto-generated method stub
	        super.onPause();

	        mediaPlayer.pause();
	       
	    }
	    @Override
	    public boolean onCreateOptionsMenu(Menu menu)
	    {
	        // Inflate the menu; this adds items to the action bar if it is present.
	        getMenuInflater().inflate(R.menu.main, menu);
	        return true;
	    }

	}
