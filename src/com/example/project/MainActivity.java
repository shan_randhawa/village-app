package com.example.project;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

public class MainActivity extends Activity {
	public int layoutpressed = -1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        manage_buttons();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    void manage_buttons(){
    	
    	final LinearLayout pub = (LinearLayout )findViewById(R.id.publish);
    	   pub.setOnTouchListener(new View.OnTouchListener() {
               @Override
               public boolean onTouch(View arg0, MotionEvent arg1) {
               if (arg1.getAction()==MotionEvent.ACTION_DOWN){
            	   pub.setBackgroundResource(R.drawable.pressed);
            	   Button b=(Button)findViewById(R.id.b1);
            	   b.setTextSize(23);
            	  
     	           
                       layoutpressed = arg0.getId();
               }
               else if (arg1.getAction()== MotionEvent.ACTION_UP){
            	   pub.setBackgroundResource(R.drawable.button_gradient);
            	  
                      
                    	 
                       
         }
         else{
              
               layoutpressed = -1;
         }
         return true;
               }
 }); 
    	   
    	   final LinearLayout read = (LinearLayout )findViewById(R.id.read);
    	   read.setOnTouchListener(new View.OnTouchListener() {
               @Override
               public boolean onTouch(View arg0, MotionEvent arg1) {
               if (arg1.getAction()==MotionEvent.ACTION_DOWN){
            	   read.setBackgroundResource(R.drawable.pressed);
            	   
            	   Button b=(Button)findViewById(R.id.b2);
            	   b.setTextSize(23);
                       layoutpressed = arg0.getId();
               }
               else if (arg1.getAction()== MotionEvent.ACTION_UP){
            	   read.setBackgroundResource(R.drawable.button_gradient);
            	  
            	   Intent i = new Intent();
					i.setClass(MainActivity.this, Sections.class);
					startActivity(i);
                       
         }
         else{
              
               layoutpressed = -1;
         }
         return true;
               }
 }); 
    }
    
}
