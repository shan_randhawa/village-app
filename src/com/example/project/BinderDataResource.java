package com.example.project;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class BinderDataResource extends BaseAdapter {

	
	
	LayoutInflater inflater;
	List<String> DataCollection;
	List<String> showheaders;
	ViewHolder holder;
	public BinderDataResource() {
		// TODO Auto-generated constructor stub
	}
	
	public BinderDataResource(Activity act, List<String> header, List<String> pdfnames) {
		
		this.DataCollection = pdfnames;
		this.showheaders=header;
		
		inflater = (LayoutInflater) act
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	

	public int getCount() {
		// TODO Auto-generated method stub
//		return idlist.size();
		return DataCollection.size();
	}

	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		 
		View vi=convertView;
	    if(convertView==null){
	     
	      vi = inflater.inflate(R.layout.list_items_resource, null);
	      holder = new ViewHolder();
	     
	      holder.header = (TextView)vi.findViewById(R.id.separator); //  name
	      holder.pdf_name =(TextView)vi.findViewById(R.id.title); // image
	 
	      vi.setTag(holder);
	    }
	    else{
	    	
	    	holder = (ViewHolder)vi.getTag();
	    }

	      // Setting all values in listview
	      
	      
	      if(showheaders.get(position)=="-")
	      {
	    	  holder.header.setVisibility(View.GONE) ;
	    	  
	    	  holder.pdf_name.setText(DataCollection.get(position));
	      }
	      else
	      {
	    	  holder.header.setVisibility(View.VISIBLE) ;
	    	  holder.pdf_name.setText(DataCollection.get(position));
	    	  holder.header.setText(showheaders.get(position));
	      }
	      
	     
	      
	      return vi;
	}
	
	/*
	 * 
	 * */
	static class ViewHolder{
		
		TextView header;
		TextView pdf_name;
		
	}
	
}
