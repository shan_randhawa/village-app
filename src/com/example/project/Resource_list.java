package com.example.project;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;


public class Resource_list extends Activity {
	
	String section = "";
	String section_id = "";
	ListView list;
	View popupView;
	public int layoutpressed = -1;
	
	
    @SuppressWarnings("deprecation")
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_resource);
        
       
		try {
			
		
			
			 //Get position to display
			 Intent intent = getIntent();
		        
		        this.section = intent.getStringExtra("section"); 
		        this.section_id = intent.getStringExtra("section_id");
	        final TextView btnOpenPopup = (TextView) findViewById(R.id.bookss);
			View anyView = findViewById(R.id.bookss);
					anyView.post(new Runnable()
					{
					    @Override
					    public void run()
					    {
					    	LayoutInflater layoutInflater = 
									(LayoutInflater)getBaseContext()
									.getSystemService(LAYOUT_INFLATER_SERVICE);
						   popupView = layoutInflater.inflate(R.layout.popup, null);
							final PopupWindow popupWindow = new PopupWindow(
									popupView, 400, 330);
							
							
							final LinearLayout nne = (LinearLayout )popupView.findViewById(R.id.neew);
							 LinearLayout ext= (LinearLayout )popupView.findViewById(R.id.exist);
								
								ext.setOnClickListener(new Button.OnClickListener(){

									@Override
									public void onClick(View v) {
										
										popupWindow.dismiss();
										TextView tt=   (TextView)findViewById(R.id.bookss);
						                   tt.setVisibility(View.VISIBLE);
						                   runit();
									}});

								nne.setOnClickListener(new Button.OnClickListener(){

									@Override
									public void onClick(View v) {
										popupWindow.dismiss();
										TextView tt=   (TextView)findViewById(R.id.bookss);
						                   tt.setVisibility(View.VISIBLE);
										getNew();
									}});
					    	   
					    	   
					    	   
					    	   
							popupWindow.showAsDropDown(btnOpenPopup, 25, -30);
					        // Create and show PopupWindow
					    }
					});
			
			
	    	   
          

          
		
			
		}
		
		catch (Exception ex) {
			Log.e("Error", "Loading exception");
		}
		
    }
   
   void getNew(){
	   List<String> status = new ArrayList<String>();
	   final List<String> titles = new ArrayList<String>();
	   JsonParser.handleBooksBySection(this,section_id,section);
	   
	   File file= new File(Environment.getExternalStorageDirectory()+"/VillageApp"+"/"+section+"/titles.txt");
	   try {
	         BufferedReader br = new BufferedReader(new FileReader(file));
	         String line;

	         while ((line = br.readLine()) != null) {
	        	 titles.add(line);
	         }
	         br.close();
	     }
	     catch (IOException e) {
	         //You'll need to add proper error handling here
	     }
	   
	   
	   file= new File(Environment.getExternalStorageDirectory()+"/VillageApp"+"/"+section+"/downloading.txt");
	   if(file.exists())
	   {
		   try {
		         BufferedReader br = new BufferedReader(new FileReader(file));
		         String line;

		         while ((line = br.readLine()) != null) {
		        	 status.add(line);
		         }
		         br.close();
		     }
		     catch (IOException e) {
		         //You'll need to add proper error handling here
		     }
	   }
	   else
	   {
		   for(int i=0;i<titles.size();i++)
		   {
			   status.add("-");
		   }
		   
	   }
	   
	   BinderDataBooks bindingData = new BinderDataBooks(this,status,titles);
       list = (ListView) findViewById(R.id.list);
       list.setAdapter(bindingData);
   	list.setOnItemClickListener(new OnItemClickListener() {

		public void onItemClick(AdapterView<?> parent, View view,
				int position, long id) {

			    Intent i = new Intent();
			
			
				i.setClass(Resource_list.this, Show_Selected.class);
           
				 //parameters
				String k=titles.get(position);
				i.putExtra("Book_title",k);
				i.putExtra("Section",section);
				startActivity(i);
			
			
	       
		}
	});
	   
	   
   }
   void runit(){
	   
	   final List<String> fruits = new ArrayList<String>(20);
       List<String> headers = new ArrayList<String>(20);
       
       
       File sdcard = Environment.getExternalStorageDirectory();

     //Get the text file
     File file = new File(sdcard,section +".txt");

    

     try {
         BufferedReader br = new BufferedReader(new FileReader(file));
         String line;

         while ((line = br.readLine()) != null) {
             fruits.add(line);
         }
         br.close();
     }
     catch (IOException e) {
         //You'll need to add proper error handling here
     }
       
       Collections.sort(fruits, String.CASE_INSENSITIVE_ORDER);
       headers.add(""+fruits.get(0).charAt(0));
       for(int i=1,j=i-1;i<fruits.size();i++,j++)
       {
       	
       	if(Character.toLowerCase(fruits.get(i).charAt(0))==Character .toLowerCase(fruits.get(j).charAt(0)))
       	{
       		 headers.add("-");
       	}
       	else
       	{
       		headers.add(""+fruits.get(i).charAt(0));
       		
       	}
       }
       
       BinderDataResource bindingData = new BinderDataResource(this,headers,fruits);
       list = (ListView) findViewById(R.id.list);
       list.setAdapter(bindingData);
   	list.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				    Intent i = new Intent();
				
				
					i.setClass(Resource_list.this, Read_Selected.class);
               
					 //parameters
					String k=fruits.get(position);
					
					i.putExtra("Topic_name",k);
					startActivity(i);
				
				
		       
			}
		});
   }
}
