package com.example.project;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;



public class Download {


    public static void createDir(String path,String dirName)
    {
          String newFolder = "/"+dirName;
          File myNewFolder = new File(path + newFolder);
          myNewFolder.mkdir();
    }

    public void downloadEventData(Context context,String zipFile,String unzipLocation,String url) throws IOException
    {
        try {
                new DownloadMapAsync(context,zipFile,unzipLocation).execute(url);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
        private class DownloadMapAsync extends AsyncTask<String, String, String> {
            String result ="";
            Context context;
            String zipFile;
            String unzipLocation;
            private ProgressDialog progressDialog;
            String string;
            public DownloadMapAsync(Context context,String zipFile,String unzipLocation) {
                // TODO Auto-generated constructor stub
                this.context=context;
                this.zipFile=zipFile;
                this.unzipLocation=unzipLocation;
            }
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = new ProgressDialog(context);
                progressDialog.setMessage("Loading...");
                progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                progressDialog.setCancelable(false);
                progressDialog.show();
            }

            @Override
            protected String doInBackground(String... aurl) {
                int count;
                HttpURLConnection http = null;
            try {
                URL url = new URL(aurl[0]);
                if (url.getProtocol().toLowerCase().equals("https")) {
                    HttpsURLConnection https = (HttpsURLConnection) url.openConnection();
                    http = https;
                } else {
                    http = (HttpURLConnection) url.openConnection();
                }
            http.connect();
            if (http.getResponseCode()==200)
            {
                int lenghtOfFile = http.getContentLength();
                InputStream input = new BufferedInputStream(url.openStream());

                OutputStream output = new FileOutputStream(zipFile);

                byte data[] = new byte[1024];
                long total = 0;

                    while ((count = input.read(data)) != -1) {
                        total += count;
                        publishProgress(""+(int)((total*100)/lenghtOfFile));
                        output.write(data, 0, count);
                    }
                    output.close();
                    input.close();
                    result = "true";
            } 
            else if (http.getResponseCode()==401)
            {
                result = "false";
                string= "Download Limit exceed.";   
            }  
            else 
            {
                result = "false";
                string=http.getResponseMessage();
            }

            } catch (Exception e) {
                e.printStackTrace();
                result = "false";
                try {
                    if (http.getResponseCode()==401)
                    {
                        string= "Download Failed";  
                    } else {
                        string=e.toString();
                    }

                } catch (IOException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
            return result;

            }
            protected void onProgressUpdate(String... progress) {
                 Log.d("ANDRO_ASYNC",progress[0]);
                 progressDialog.setProgress(Integer.parseInt(progress[0]));
            }

            @Override
            protected void onPostExecute(String unused) {
                progressDialog.dismiss();
               
                
                
            }
        }
	
	
}
