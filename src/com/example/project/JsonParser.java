package com.example.project;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

public class JsonParser {
	

	static HttpClient httpclient = null;
    static HttpPost httppost = null;
    static HttpResponse response =null;
	
	@SuppressWarnings("deprecation")
	public static boolean handleSections(Context context){
		// Create a new HttpClient and Post Header
		 httpclient = new DefaultHttpClient();
	     httppost = new HttpPost(Constants.BASE_URL + "/index.php/webapp/get_all_categories");
	   List<String> ids = new ArrayList<String>();
	   List<String> categories = new ArrayList<String>();
	   List<String> thumbs_links = new ArrayList<String>();
	    try {
	    	 // Adding Paramters
	        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
	        nameValuePairs.add(new BasicNameValuePair("token",Constants.TOKEN));
	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
 
	        thread.start();
	        
	        // CONVERT RESPONSE TO STRING
	        while(response==null)
	        {
	        	
	        }
	        
            String result = EntityUtils.toString(response.getEntity());
             
            
           
            // CONVERT RESPONSE STRING TO JSON ARRAY
            JSONArray ja = new JSONArray(result);
 
            // ITERATE THROUGH AND RETRIEVE FIELDS
            int n = ja.length();
            for (int i = 0; i < n; i++) {
                // GET INDIVIDUAL JSON OBJECT FROM JSON ARRAY
                JSONObject jo = ja.getJSONObject(i);
                 
                // RETRIEVE EACH JSON OBJECT'S FIELDS
                String c_id = jo.getString("c_id");
                String category = jo.getString("category");
                //String thumb = jo.getString("c_thumb");
                 
                ids.add(c_id);
                categories.add(category);
                thumbs_links.add("https://www.iconfinder.com/icons/359466/download/png/128");
                // writng  DATA  TO files
               
            }
            
            File_manip.ManageSection(context,ids, categories, thumbs_links);

	    } 
	    catch (ClientProtocolException e) {
	        // TODO Auto-generated catch block
	    }
	    catch (IOException e) {
	        // TODO Auto-generated catch block
	    } catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
		return true;
		
		
	}
	
	
	
	public static boolean handleBooksBySection(Context context,String category_id,String section){
		// Create a new HttpClient and Post Header
		 response=null;
		 httpclient = new DefaultHttpClient();
	     httppost = new HttpPost(Constants.BASE_URL + "/index.php/webapp/search");
	   List<String> book_ids = new ArrayList<String>();
	   List<String> titles = new ArrayList<String>();
	   List<String> authors = new ArrayList<String>();
	   List<String> languages = new ArrayList<String>();
	   List<String> thumbs_links = new ArrayList<String>();
	    try {
	    	 // Adding Paramters
	        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
	        nameValuePairs.add(new BasicNameValuePair("token",Constants.TOKEN));
	        nameValuePairs.add(new BasicNameValuePair("title",""));
	        nameValuePairs.add(new BasicNameValuePair("author",""));
	        nameValuePairs.add(new BasicNameValuePair("language","all"));
	        nameValuePairs.add(new BasicNameValuePair("trans_language","all"));
	        nameValuePairs.add(new BasicNameValuePair("category",category_id));
	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
 
	        thread2.start();
	        
	        // CONVERT RESPONSE TO STRING
	        while(response==null)
	        {
	        	
	        }
            String result = EntityUtils.toString(response.getEntity());
             
            // CONVERT RESPONSE STRING TO JSON ARRAY
            JSONArray ja = new JSONArray(result);
 
            // ITERATE THROUGH AND RETRIEVE FIELDS
            int n = ja.length();
            for (int i = 0; i < n; i++) {
                // GET INDIVIDUAL JSON OBJECT FROM JSON ARRAY
                JSONObject jo = ja.getJSONObject(i);
                 
                // RETRIEVE EACH JSON OBJECT'S FIELDS
                String c_id = jo.getString("b_id");
                String title = jo.getString("title");
                String thumb = jo.getString("thumb");
                String author = jo.getString("author");
                String language = jo.getString("language");
                //String thumb = jo.getString("c_thumb");
                 
                book_ids.add(c_id);
                titles.add(title);
                thumbs_links.add(thumb);
                authors.add(author);
                languages.add(language);
                // writng  DATA  TO files
               
            }
            
            File_manip.ManageSectionBooks(context,section,book_ids, titles, thumbs_links,authors,languages);

	    } 
	    catch (ClientProtocolException e) {
	        // TODO Auto-generated catch block
	    }
	    catch (IOException e) {
	        // TODO Auto-generated catch block
	    } catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
		return true;
		
		
	}
	
	
	
	static Thread thread = new Thread(new Runnable(){
	    @Override
	    public void run() {
	        try {
	        	// Execute HTTP Post Request
		         response = httpclient.execute(httppost);
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }
	});
	
	
	static Thread thread2 = new Thread(new Runnable(){
	    @Override
	    public void run() {
	        try {
	        	// Execute HTTP Post Request
		         response = httpclient.execute(httppost);
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }
	});
	

}


