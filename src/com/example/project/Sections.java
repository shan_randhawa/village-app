package com.example.project;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class Sections extends Activity {

    

    private List<String> section_names;
    private List<String> section_ids;
    
    ListView list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        if(Availabilies.haveNetworkConnection(getApplicationContext())==true)
        {
        	
        	
        	if(Availabilies.isExternalStorageAvailableAndWriteable()==true)
        	{
        		
        		Utils.createDir(Environment.getExternalStorageDirectory().toString(),"State");      		
        	    JsonParser.handleSections(this);
        	    getnames();
                managelistview();
        	    
        	}
        	else if(Availabilies.checkAvailableInternal()==true)
        	{
        	
        	}
        }

    }
    void managelistview(){
    	
    	BinderData bindingData = new BinderData(this,section_names);
        list = (ListView) findViewById(R.id.list);
        list.setAdapter(bindingData);
        
     // Click event for single list row
     			list.setOnItemClickListener(new OnItemClickListener() {

     				public void onItemClick(AdapterView<?> parent, View view,
     						int position, long id) {

     					Intent i = new Intent();
     					i.setClass(Sections.this, Resource_list.class);
                       
     					// parameters
     					i.putExtra("section",section_names.get(position));
     					
     				
                      
     					String s= section_names.get(position);
     					String b_id=null;
     					for(int _i=0;_i<section_names.size();_i++)
     					{
     						if(section_names.get(_i)==s)
     						{
     							b_id=section_ids.get(_i);
     							i.putExtra("section_id",b_id);
     							break;
     						}
     					}
     			        Download.createDir(Environment.getExternalStorageDirectory().toString(),"VillageApp");
     			        Download.createDir(Environment.getExternalStorageDirectory().toString()+"/VillageApp", section_names.get(position));
     					startActivity(i);
     				}
     			});
    }
    void getnames(){
        section_ids=File_manip.get_bok_id_name(this,"Sectionsids.txt");
        section_names=File_manip.get_bok_id_name(this,"SectionsNames.txt");
    }

	
    
}