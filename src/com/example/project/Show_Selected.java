
	package com.example.project;

	import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;
import android.R.bool;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.ViewTreeObserver;
import android.webkit.WebSettings.ZoomDensity;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.view.MotionEvent;



	public class Show_Selected extends Activity
	{

	    //Globals:
	    private ImageView cover;
	    private TextView titl;
	    private TextView auth;
	    private TextView lang;
	    
	    private int ViewSize = 0;
		String Book = "";
		String section = "";
	  
	    //OnCreate Method:
	    @Override
	    protected void onCreate(Bundle savedInstanceState)
	    {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.activity_book_detail);
	        
	        
	        Intent intent = getIntent();
	        
	        this.Book = intent.getStringExtra("Book_title");
	        this.section=intent.getStringExtra("Section");
	        fillData();
            

	    }

	   void fillData(){
		   int i=0;
		   String author = "";
		   String language = "";
		   File file= new File(Environment.getExternalStorageDirectory()+"/VillageApp"+"/"+section+"/titles.txt");
		   try {
		         BufferedReader br = new BufferedReader(new FileReader(file));
		         String line;

		         while ((line = br.readLine()) != null) {
		        	 if(Book.equals(line))
		        	 
		        		 break;
		        	 
		        	 else
		        		i++;
		        	 
		         }
		         br.close();
		     }
		     catch (IOException e) {
		         //You'll need to add proper error handling here
		     }
		   int j=0;
		   file= new File(Environment.getExternalStorageDirectory()+"/VillageApp"+"/"+section+"/authors.txt");
		   try {
		         BufferedReader br = new BufferedReader(new FileReader(file));
		         String line;

		         while ((line = br.readLine()) != null) {
		        	 if(j==i)
		        	 {
		        		 author=line;
		        	 break;
		        	 }
		        	 else
		        		j++;
		        	 
		         }
		         br.close();
		     }
		     catch (IOException e) {
		         //You'll need to add proper error handling here
		     }
		   
		   j=0;
		   file= new File(Environment.getExternalStorageDirectory()+"/VillageApp"+"/"+section+"/languages.txt");
		   try {
		         BufferedReader br = new BufferedReader(new FileReader(file));
		         String line;

		         while ((line = br.readLine()) != null) {
		        	 if(j==i)
		        	 {
		        		 language=line;
		        	 break;
		        	 }
		        	 else
		        		j++;
		        	 
		         }
		         br.close();
		     }
		     catch (IOException e) {
		         //You'll need to add proper error handling here
		     }
		   
		   cover =(ImageView)findViewById(R.id.book_cover);
		   titl = (TextView)findViewById(R.id.book_title);
		   auth = (TextView)findViewById(R.id.Author_name);
		   lang = (TextView)findViewById(R.id.Language_name);
		   
		   String path =Environment.getExternalStorageDirectory() +"/VillageApp"+"/"+section+"/"+Book+".png" ; 
		      Bitmap bmp = BitmapFactory.decodeFile(path);
		      cover.setImageBitmap(bmp);
		      
		     titl.setText(Book);
		     auth.setText(author);
		     lang.setText(language);
		   
	   }
	    @Override
	    public boolean onCreateOptionsMenu(Menu menu)
	    {
	        // Inflate the menu; this adds items to the action bar if it is present.
	        getMenuInflater().inflate(R.menu.main, menu);
	        return true;
	    }

	}
